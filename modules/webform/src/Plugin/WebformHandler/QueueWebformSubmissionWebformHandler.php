<?php

namespace Drupal\entity_counter_webform\Plugin\WebformHandler;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\entity_counter\CounterTransactionOperation;
use Drupal\entity_counter\EntityCounterStatus;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queues a webform submission.
 *
 * @WebformHandler(
 *   id = "entity_counter_webform_queue_webform_submission",
 *   label = @Translation("Queue a webform submission"),
 *   category = @Translation("Entity Counter"),
 *   description = @Translation("Queues a webform submission to evaluate."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 *   tokens = TRUE,
 * )
 */
class QueueWebformSubmissionWebformHandler extends WebformHandlerBase {

  /**
   * Flag indicating if there are entity counters.
   *
   * @var bool
   */
  protected $counters;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The queue plugin manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\entity_counter_webform\Plugin\WebformHandler\QueueWebformSubmissionWebformHandler $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setTokenManager($container->get('webform.token_manager'));
    $instance->setQueueManager($container->get('plugin.manager.queue_worker'));
    $instance->setQueue($container->get('queue'));
    $instance->counters = (bool) $container->get('entity_type.manager')
      ->getStorage('entity_counter')
      ->getQuery()
      ->condition('status', [EntityCounterStatus::OPEN, EntityCounterStatus::MAX_UPPER_LIMIT], 'IN')
      ->count()
      ->execute();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'queue' => 'entity_counter_evaluate_entity',
      'operation' => CounterTransactionOperation::ADD,
      'webform_submission_id' => '[webform_submission:sid]',
      'states' => [WebformSubmissionInterface::STATE_COMPLETED],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->applyFormStateToConfiguration($form_state);

    // Queue settings.
    // From.
    $form['queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Queue settings'),
      '#open' => TRUE,
    ];
    $form['queue']['queue'] = [
      '#type' => 'select',
      '#title' => $this->t('Queue'),
      '#options' => $this->getQueues(),
      '#default_value' => $this->configuration['queue'],
      '#required' => TRUE,
    ];
    $form['queue']['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => [
        CounterTransactionOperation::ADD => $this->t('Add'),
        CounterTransactionOperation::CANCEL => $this->t('Cancel'),
      ],
      '#default_value' => $this->configuration['operation'],
      '#required' => TRUE,
    ];
    $form['queue']['webform_submission_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform submission ID'),
      '#default_value' => $this->configuration['webform_submission_id'],
      '#required' => TRUE,
    ];

    // Additional.
    $form['additional'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional settings'),
    ];
    // Settings: States.
    $form['additional']['states'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send email'),
      '#options' => [
        WebformSubmissionInterface::STATE_DRAFT => $this->t('…when <b>draft</b> is saved.'),
        WebformSubmissionInterface::STATE_CONVERTED => $this->t('…when anonymous submission is <b>converted</b> to authenticated.'),
        WebformSubmissionInterface::STATE_COMPLETED => $this->t('…when submission is <b>completed</b>.'),
        WebformSubmissionInterface::STATE_UPDATED => $this->t('…when submission is <b>updated</b>.'),
        WebformSubmissionInterface::STATE_DELETED => $this->t('…when submission is <b>deleted</b>.'),
      ],
      '#default_value' => $this->configuration['states'],
    ];
    $form['additional']['states_message'] = [
      '#type' => 'webform_message',
      '#message_message' => $this->t("Because no submission state is checked, this email can only be sent using the 'Resend' form and/or custom code."),
      '#message_type' => 'warning',
    ];

    // ISSUE: TranslatableMarkup is breaking the #ajax.
    // WORKAROUND: Convert all Render/Markup to strings.
    WebformElementHelper::convertRenderMarkupToStrings($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();

    // Cleanup states.
    $values['states'] = array_values(array_filter($values['states']));

    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        $this->configuration[$name] = $values[$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $settings = $this->getConfiguration()['settings'];

    // Set state.
    $states = [
      WebformSubmissionInterface::STATE_DRAFT => $this->t('Draft Saved'),
      WebformSubmissionInterface::STATE_CONVERTED => $this->t('Converted'),
      WebformSubmissionInterface::STATE_COMPLETED => $this->t('Completed'),
      WebformSubmissionInterface::STATE_UPDATED => $this->t('Updated'),
      WebformSubmissionInterface::STATE_DELETED => $this->t('Deleted'),
    ];
    $settings['states'] = array_intersect_key($states, array_combine($settings['states'], $settings['states']));

    // Set queue.
    $settings['queue'] = $this->getQueues()[$settings['queue']];

    switch ($settings['operation']) {
      case CounterTransactionOperation::ADD:
        $settings['operation'] = $this->t('Add');
        break;

      case CounterTransactionOperation::CANCEL:
        $settings['operation'] = $this->t('Cancel');
        break;
    }

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if ($this->configuration['states'] && in_array($webform_submission->getState(), $this->configuration['states'])) {
      $this->queue($webform_submission);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    if (in_array(WebformSubmissionInterface::STATE_DELETED, $this->configuration['states'])) {
      $this->queue($webform_submission);
    }
  }

  /**
   * Queue.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   */
  protected function queue(WebformSubmissionInterface $webform_submission) {
    // Exit if no active counters exists.
    if (!$this->counters) {
      return;
    }

    try {
      $submission_id = $this->tokenManager->replace($this->getConfiguration()['settings']['webform_submission_id'], $webform_submission, [], ['clear' => TRUE]);
      if (!empty($submission_id)) {
        $this->queue
          ->get($this->getConfiguration()['settings']['queue'], TRUE)
          ->createItem([
            'operation' => $this->getConfiguration()['settings']['operation'],
            'entity_type_id' => 'webform_submission',
            'entity_id' => $submission_id,
          ]);
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_counter_webform', $exception);
    }
  }

  /**
   * Gets the queue list as #options array.
   *
   * @return array
   *   The queue list as #options array.
   */
  protected function getQueues() {
    $queues = [];

    foreach ($this->queueManager->getDefinitions() as $name => $queue_definition) {
      $queues[$name] = $queue_definition['title'];
    }

    return $queues;
  }

  /**
   * Sets the webform token manager.
   *
   * @param \Drupal\webform\WebformTokenManagerInterface $token_manager
   *   The webform token manager.
   *
   * @return $this
   */
  private function setTokenManager(WebformTokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;

    return $this;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  private function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;

    return $this;
  }

  /**
   * Sets the queue factory.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   *
   * @return $this
   */
  private function setQueue(QueueFactory $queue) {
    $this->queue = $queue;

    return $this;
  }

  /**
   * Sets the queue plugin manager.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue plugin manager.
   *
   * @return $this
   */
  private function setQueueManager(QueueWorkerManagerInterface $queue_manager) {
    $this->queueManager = $queue_manager;

    return $this;
  }

}
