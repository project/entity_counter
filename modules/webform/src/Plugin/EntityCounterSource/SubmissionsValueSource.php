<?php

namespace Drupal\entity_counter_webform\Plugin\EntityCounterSource;

use Drupal\entity_counter_webform\Plugin\WebformValueEntityCounterSource;

/**
 * Adds webform submissions value source to entity counters.
 *
 * @EntityCounterSource(
 *   id = "entity_counter_webform_submissions_value",
 *   label = @Translation("Webform submissions value source"),
 *   description = @Translation("Sum the webform submissions value."),
 *   cardinality = \Drupal\entity_counter\EntityCounterSourceCardinality::UNLIMITED,
 *   value_type = \Drupal\entity_counter\EntityCounterSourceValue::INCREMENTAL
 * )
 */
class SubmissionsValueSource extends WebformValueEntityCounterSource {

  /**
   * {@inheritdoc}
   */
  protected function getConditionOptionsForm() {
    static $options = NULL;

    if ($options === NULL) {
      foreach ($this->conditionManager->getFilteredDefinitions(['webform_submission']) as $plugin_id => $definition) {
        $options[$plugin_id] = (string) $definition['label'];
      }
    }

    return $options;
  }

}
