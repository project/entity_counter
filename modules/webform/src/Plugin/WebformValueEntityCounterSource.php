<?php

namespace Drupal\entity_counter_webform\Plugin;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a webform value class for an entity counter source.
 */
class WebformValueEntityCounterSource extends WebformEntityCounterSourceBase implements WebformValueEntityCounterSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The webform field that this source should use to do the sum.
      'webform_field' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['webform'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webform field'),
    ];
    $form['webform']['webform_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->getWebformField(),
      '#required' => TRUE,
      '#parents' => [
        'settings',
        'webform_field',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebformField() {
    return $this->configuration['webform_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function setWebformField($name) {
    $this->configuration['webform_field'] = $name;

    return $this;
  }

}
