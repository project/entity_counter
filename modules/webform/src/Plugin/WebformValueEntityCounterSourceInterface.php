<?php

namespace Drupal\entity_counter_webform\Plugin;

use Drupal\entity_counter\Plugin\EntityCounterSourceWithEntityConditionsInterface;

/**
 * Defines the interface for webform value entity counter sources.
 *
 * @see \Drupal\entity_counter\Annotation\EntityCounterSource
 * @see \Drupal\entity_counter_webform\Plugin\WebformValueEntityCounterSource
 * @see \Drupal\entity_counter\Plugin\EntityCounterSourceManagerInterface
 * @see plugin_api
 */
interface WebformValueEntityCounterSourceInterface extends EntityCounterSourceWithEntityConditionsInterface {

  /**
   * Retrieves the name of the webform field.
   *
   * @return string
   *   The name of the webform field.
   */
  public function getWebformField();

  /**
   * Sets the name of the webform field.
   *
   * @param string $name
   *   The name of the webform field.
   *
   * @return $this
   */
  public function setWebformField($name);

}
