<?php

namespace Drupal\entity_counter_commerce\Plugin\CommerceReaction;

use Drupal\commerce_reactions\Entity\ReactionInterface;
use Drupal\commerce_reactions\Plugin\CommerceReactionBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\entity_counter\CounterTransactionOperation;
use Drupal\entity_counter\EntityCounterStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides "Queue an order" reaction.
 *
 * @CommerceReaction(
 *   id = "entity_counter_commerce_queue_order",
 *   label = @Translation("Queue an order"),
 *   category = @Translation("Entity Counter"),
 *   entity_types = {"commerce_order"}
 * )
 */
class QueueOrder extends CommerceReactionBase implements ContainerFactoryPluginInterface {

  /**
   * Flag indicating if there are entity counters.
   *
   * @var bool
   */
  protected $counters;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The queue plugin manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\entity_counter_commerce\Plugin\CommerceReaction\QueueOrder $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setQueueManager($container->get('plugin.manager.queue_worker'));
    $instance->setQueue($container->get('queue'));
    $instance->counters = (bool) $container->get('entity_type.manager')
      ->getStorage('entity_counter')
      ->getQuery()
      ->condition('status', [EntityCounterStatus::OPEN, EntityCounterStatus::MAX_UPPER_LIMIT], 'IN')
      ->count()
      ->execute();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'queue' => 'entity_counter_evaluate_entity',
      'operation' => CounterTransactionOperation::ADD,
      'entity_id' => '[commerce_order:order_id]',
      'entity_type' => 'commerce_order',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $tab_group = implode('][', array_merge($form['#parents'], ['configuration']));

    $form['configuration'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Configuration'),
    ];

    // Queue.
    $form['queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Queue'),
      '#group' => $tab_group,
    ];
    $form['queue']['queue'] = [
      '#type' => 'select',
      '#title' => $this->t('Queue'),
      '#options' => $this->getQueues(),
      '#default_value' => $this->getConfiguration()['queue'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['queue']),
    ];
    $form['queue']['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => [
        CounterTransactionOperation::ADD => $this->t('Add'),
        CounterTransactionOperation::CANCEL => $this->t('Cancel'),
      ],
      '#default_value' => $this->getConfiguration()['operation'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['operation']),
    ];
    $form['queue']['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity ID'),
      '#default_value' => $this->getConfiguration()['entity_id'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['entity_id']),
    ];
    $form['queue']['entity_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity type'),
      '#default_value' => $this->getConfiguration()['entity_type'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['entity_type']),
    ];

    return $form;
  }

  /**
   * Queue.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   * @param \Drupal\commerce_reactions\Entity\ReactionInterface $reaction
   *   The reaction entity.
   */
  protected function doExecute(array $contexts, ReactionInterface $reaction) {
    // Exit if no active counters exists.
    if (!$this->counters) {
      return;
    }

    parent::doExecute($contexts, $reaction);

    try {
      $entity_id = $this->tokenManager->replace($this->getConfiguration()['entity_id'], $this->tokenData, $this->tokenOptions);
      $entity_type = $this->tokenManager->replace($this->getConfiguration()['entity_type'], $this->tokenData, $this->tokenOptions);
      if (!empty($entity_id)) {
        $this->queue
          ->get($this->getConfiguration()['queue'], TRUE)
          ->createItem([
            'operation' => $this->getConfiguration()['operation'],
            'entity_type_id' => $entity_type,
            'entity_id' => $entity_id,
          ]);
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_counter_commerce', $exception);
    }
  }

  /**
   * Gets the queue list as #options array.
   *
   * @return array
   *   The queue list as #options array.
   */
  protected function getQueues() {
    $queues = [];

    foreach ($this->queueManager->getDefinitions() as $name => $queue_definition) {
      $queues[$name] = $queue_definition['title'];
    }

    return $queues;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  private function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;

    return $this;
  }

  /**
   * Sets the queue factory.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   *
   * @return $this
   */
  private function setQueue(QueueFactory $queue) {
    $this->queue = $queue;

    return $this;
  }

  /**
   * Sets the queue plugin manager.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_manager
   *   The queue plugin manager.
   *
   * @return $this
   */
  private function setQueueManager(QueueWorkerManagerInterface $queue_manager) {
    $this->queueManager = $queue_manager;

    return $this;
  }

}
