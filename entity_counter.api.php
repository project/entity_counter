<?php

/**
 * @file
 * Hooks and documentation related to entity counter module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Control entity operation access for a specific entity type.
 *
 * @param bool $value
 *   The default transaction value.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 * @param \Drupal\entity_counter\Plugin\EntityCounterSourceWithEntityConditionsInterface $source
 *   The entity counter source.
 *
 * @see \Drupal\entity_counter\Plugin\QueueWorker\EntityCounterEvaluateEntityQueueWorker::processItem
 */
function hook_ENTITY_COUNTER_SOURCE_PLUGIN_ID_alter(&$value, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\entity_counter\Plugin\EntityCounterSourceWithEntityConditionsInterface $source) {
  if ($entity instanceof \Drupal\commerce_order\Entity\Order) {
    $value = $entity->getTotalPrice() ? $entity->getTotalPrice()->getNumber() : 0;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
