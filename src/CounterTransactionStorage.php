<?php

namespace Drupal\entity_counter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the storage handler class for counter transactions.
 */
class CounterTransactionStorage extends SqlContentEntityStorage {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->queue = $container->get('queue');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doCreate(array $values) {
    if (!isset($values['entity_counter'])) {
      throw new EntityStorageException('Missing entity counter for this counter transaction.');
    }
    $entity = parent::doCreate($values);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function doPreSave(EntityInterface $entity) {
    $return = parent::doPreSave($entity);

    /** @var \Drupal\entity_counter\Entity\CounterTransactionInterface $entity */
    if (!$entity->getEntityCounterSourceId()) {
      throw new EntityStorageException('Missing entity counter source for this counter transaction.');
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  protected function doPostSave(EntityInterface $entity, $update) {
    /** @var \Drupal\entity_counter\Entity\CounterTransactionInterface $entity */
    /** @var \Drupal\entity_counter\Entity\CounterTransactionInterface $original_entity */
    $original_entity = $entity->original;

    if ((empty($original_entity) && $entity->isQueued()) ||
      ($entity->isQueued() && !$original_entity->isQueued()) ||
      ($entity->getOperation() == CounterTransactionOperation::CANCEL && $original_entity->getOperation() != CounterTransactionOperation::CANCEL)) {
      $this->queue->get('entity_counter_transaction', TRUE)->createItem(['revision_id' => $entity->getRevisionId()]);
    }

    parent::doPostSave($entity, $update);
  }

}
