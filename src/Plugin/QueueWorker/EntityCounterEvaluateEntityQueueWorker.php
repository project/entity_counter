<?php

namespace Drupal\entity_counter\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\entity_counter\CounterTransactionOperation;
use Drupal\entity_counter\EntityCounterStatus;
use Drupal\entity_counter\Exception\EntityCounterException;
use Drupal\entity_counter\Plugin\EntityCounterSourceWithEntityConditionsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process entity counter transaction queue tasks.
 *
 * @QueueWorker(
 *   id = "entity_counter_evaluate_entity",
 *   title = @Translation("Evaluate an entity to queue a transaction"),
 *   cron = {"time" = 60}
 * )
 */
class EntityCounterEvaluateEntityQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The loaded entity counters.
   *
   * @var \Drupal\entity_counter\Entity\EntityCounterInterface[]
   */
  protected $counters;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new EntityCounterEvaluateEntityQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManager $entity_type_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->counters = $entity_type_manager
      ->getStorage('entity_counter')
      ->loadByProperties([
        'status' => [EntityCounterStatus::OPEN, EntityCounterStatus::MAX_UPPER_LIMIT],
      ]);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      foreach (['operation', 'entity_type_id', 'entity_id'] as $required_value) {
        if (!isset($data[$required_value])) {
          throw new \InvalidArgumentException(sprintf('Missing required value "%s".', $required_value));
        }
      }

      $storage = $this->entityTypeManager->getStorage($data['entity_type_id']);
      $entity = $storage->load($data['entity_id']);
      if ($entity) {
        foreach ($this->counters as $counter) {
          if ($counter->isOpen()) {
            foreach ($counter->getSources() as $source) {
              if ($source->isEnabled() && $source instanceof EntityCounterSourceWithEntityConditionsInterface) {
                try {
                  $source->setConditionEntity($entity);

                  if ($source->applies() && $source->evaluateConditions()) {
                    switch ($data['operation']) {
                      case CounterTransactionOperation::ADD:
                        $transaction_value = 1.00;
                        // Allow other modules alter the transaction value.
                        $this->moduleHandler->alter($source->getPluginId(), $transaction_value, $entity, $source);
                        $source->addTransaction($transaction_value, $entity);
                        break;

                      case CounterTransactionOperation::CANCEL:
                        $source->cancelTransaction($entity);
                        break;
                    }
                  }
                }
                catch (\Exception $exception) {
                  // Nothing to do.
                }
              }
            }
          }
        }
      }
    }
    catch (EntityCounterException $exception) {
      watchdog_exception('entity_counter', $exception);
    }
  }

}
