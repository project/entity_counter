<?php

namespace Drupal\entity_counter\Exception;

/**
 * A common exception for entity counter.
 *
 * This class can be used to catch all exceptions thrown by entity counter.
 */
class EntityCounterException extends \Exception {

}
