<?php

namespace Drupal\entity_counter;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the storage class for entity counter entities.
 */
class EntityCounterStorage extends ConfigEntityStorage {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->state = $container->get('state');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doDelete($entities) {
    /** @var \Drupal\entity_counter\Entity\EntityCounterInterface[] $entities */
    // Delete the auxiliary entity counter data.
    foreach ($entities as $entity) {
      $this->state->delete('entity_counter.' . $entity->id());
    }

    parent::doDelete($entities);
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(array $ids = NULL) {
    /** @var \Drupal\entity_counter\Entity\EntityCounterInterface[] $entities */
    $entities = parent::doLoadMultiple($ids);

    // Load the auxiliary entity counter data and attach it to the entity.
    foreach ($entities as $entity) {
      $value = $this->state->get('entity_counter.' . $entity->id(), $entity->getInitialValue());
      $entity->set('value', is_array($value) ? $value['total'] : $value);
    }

    return $entities;
  }

}
