### SUMMARY
This module provides an entity counters based system to store statics.

### INSTALLATION
Using composer:
```
composer require drupal/entity_counter --sort-packages
```

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/u/pcambra)
- Manuel Egío [(facine)](http://drupal.org/u/facine)
